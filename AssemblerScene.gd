extends Control


# Hackett, a Godot engine Hack computer emulator.
# Copyright (C) 2021  Yuval Langer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


onready var source_widget: TextEdit = $MarginContainer/VBoxContainer/HBoxContainer/SourceVBoxContainer/Source
onready var decompiled_source_widget: TextEdit = $MarginContainer/VBoxContainer/HBoxContainer/ROMSoureVBoxContainer/ROMSource
onready var binary_widget: TextEdit = $MarginContainer/VBoxContainer/HBoxContainer/BinaryVBoxContainer/Binary


func _on_CompileButton_pressed() -> void:
	binary_widget.text = ''
	decompiled_source_widget.text = ''
	var source: String = source_widget.text
	var rom: PoolIntArray = Assembler.compile_source(source)
	var binary_codes: PoolStringArray = Assembler.from_rom_to_binary_code(rom)
	for binary_code in binary_codes:
		binary_widget.text += binary_code + '\n'

	var decompiled_source: PoolStringArray = Assembler.decompile(rom)
	for line in decompiled_source:
		decompiled_source_widget.text += line + '\n'


func _on_MainMenuButton_pressed() -> void:
	get_tree().change_scene("res://MainMenu.tscn")

# Hackett - An emulator for the Nand2tetris course's Hack computer implemented in the Godot game engine.

You can interact with some version of it at <https://kakafarm.itch.io/hackett>

## Assembler:

In the assembler, copy Hack assembly code from wherever and paste it into the leftmost text field. Press the compile button. The assembly code representation of the bytecodes will appear in the middle text entry field and the bytecode binary values in the rightmost text entry field.

## Emulator:

The emulator supports input, but:

* Input is written to the keyboard RAM segment even when the emulator is not running.
* Not all keys are currently recognized.

The logic side works almost to specification, maybe. Copy and paste code from:

* The [Display loop code example](https://gitlab.com/yuvallangerontheroad/hackett/-/blob/master/display_loop.asm), or
* The [Simple loop code example](https://gitlab.com/yuvallangerontheroad/hackett/-/blob/master/simple_loop.asm).

and paste it in the source entry area, and press "Run". 

## Known bugs:

* For some strange reason [clipboarding in the web version does not work](https://docs.godotengine.org/en/stable/getting_started/workflow/export/exporting_for_web.html#doc-javascript-secure-contexts) for me. It might not work for you, too. I would export for Linux and Windows, but who in their right mind would even want to run it natively?
* For some strange reason file menu won't open the native file system for me. It might not work for you, too. I would export for Linux and Windows, but… wait a minute, I suddenly have a strong sense of deja vu.

Source (AGPL-3.0-or-later): https://gitlab.com/yuvallangerontheroad/hackett/

Cover image by [José-Manuel Benito Álvarez](https://commons.wikimedia.org/wiki/File:Agarre_de_un_bifaz.png)

extends Control


# Hackett, a Godot engine Hack computer emulator.
# Copyright (C) 2021  Yuval Langer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


onready var texture_rect: TextureRect = $MarginContainer/VBoxContainer/HBoxContainer/TextureRect
onready var program_counter_label: Label = $MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/HBoxContainer/ProgramCounterValueLabel
onready var a_register_label: Label = $MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/HBoxContainer2/ARegisterValueLabel
onready var d_register_label: Label = $MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/HBoxContainer3/DRegisterValueLabel
onready var ram_display: RichTextLabel = $MarginContainer/VBoxContainer/HBoxContainer/RamDisplay
onready var rom_code_display: RichTextLabel = $MarginContainer/VBoxContainer/HBoxContainer2/VBoxContainer2/RomCodeDisplay
onready var loops_spin_box: SpinBox = $MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/HBoxContainer4/LoopsSpinBox
onready var timer: Timer = $Timer
onready var hertz_spin_box: SpinBox = $MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/HBoxContainer5/HertzSpinBox
onready var loops_per_timeout: int = loops_spin_box.value
onready var hertz: float = hertz_spin_box.value
onready var source_text_edit: TextEdit = $MarginContainer/VBoxContainer/HBoxContainer2/SourceTextEdit


var image: Image
var should_update_image: bool = false
var drawing: bool = false
var running: bool = false


#var computer_thread: Thread


func _ready() -> void:
#	computer_thread = Thread.new()

	create_image()
	update_texture()

#	print_debug(computer_thread.start(Computer, "run"))


func _on_thread_signal() -> void:
	print_debug("thread signal")


func _exit_tree() -> void:
#	computer_thread.wait_to_finish()
	print_debug("number of operations: %s" % Computer.number_of_operations_thus_far)
	print_debug(Computer.ram)


func create_image() -> void:
	image = Image.new()
	image.create(512, 256, false, Image.FORMAT_RGB8)
	image.fill(Color.white)


func update_texture() -> void:
	var texture := ImageTexture.new()
	texture.create_from_image(image)
	texture_rect.set_texture(texture)
	should_update_image = false


#func _input(event: InputEvent) -> void:
#	if event is InputEventMouseButton:
#		drawing = event.pressed
#
#	if event is InputEventMouseMotion and drawing:
#		image.lock()
#
#		image.set_pixel(event.position.x, event.position.y, Color.black)
#		should_update_image = true
#
#		image.unlock()
#
#	if event is InputEventKey:
#		pass


func _process(_delta: float) -> void:
	if running or Computer.changed_pixels:
		update_computer_values_display()
		if len(Computer.changed_pixels) > 0:
	#		print_debug(Computer.changed_pixels)
			for changed_pixel in Computer.changed_pixels:
				update_display(changed_pixel[0], changed_pixel[1])
			Computer.changed_pixels = []

	if should_update_image:
		update_texture()


func find_base_image_location(memory_address: int) -> Array:
	# XXX: Holy hell I hate doing these arithmetics…

	var relative_address: int = memory_address - Computer.DISPLAY_BASE_ADDRESS
	var x: int = (relative_address % 32) * 16
	var y: int = (relative_address / 32)
	return [x, y]


func update_display(memory_address, comp_result) -> void:
	var location: Array = find_base_image_location(memory_address)
#	print_debug("%s, %s, %s" % [memory_address - Computer.DISPLAY_BASE_ADDRESS, comp_result, location])
	var current_bit: int = 0b1
	image.lock()

	should_update_image = true

	for _i in range(16):
#		print_debug("Current bit: %s" % current_bit)
#		print_debug("Computation result: %s" % comp_result)
		if current_bit & comp_result:
			image.set_pixel(location[0], location[1], Color.black)
		else:
			image.set_pixel(location[0], location[1], Color.white)
		location[0] += 1
		current_bit = current_bit << 1

	image.unlock()


func update_computer_values_display() -> void:
	a_register_label.text = str(Computer.a_register)
	d_register_label.text = str(Computer.d_register)
	program_counter_label.text = str(Computer.program_counter)


func _on_Run_pressed() -> void:
#	Computer.running = true
	running = true
#	timer.paused = false
	var timeout_time: float = loops_per_timeout / hertz
	timer.start(timeout_time)


func _on_Pause_pressed() -> void:
	var ram = Computer.ram
	var d_register = Computer.d_register
	var a_register = Computer.a_register
	var rom = Computer.rom
	program_counter_label.text = str(Computer.program_counter)
	running = false
	timer.stop()
#	timer.paused = true
	update_rom_display()


func _on_Reset_pressed() -> void:
	create_image()
	update_texture()
	Computer.reset()
	update_computer_values_display()
	update_rom_display()


func _on_Load_Example_pressed() -> void:
	var source: String = Computer.make_cool_load_screen_source_1(14)
	source_text_edit.text = source
	var rom = Assembler.compile_source(source)
	Computer.load_rom(rom)
	Computer.reset()

func _on_LoadSource_pressed() -> void:
	var rom: PoolIntArray = Assembler.compile_source(source_text_edit.text)
	Computer.load_rom(rom)
	update_rom_display()


func update_rom_display() -> void:
	var rom_code_text = ''
	var decompiled_rom = Assembler.decompile(Computer.rom)
	for line_index in range(len(decompiled_rom)):
		if line_index == Computer.program_counter:
			rom_code_text += "%s: %s <===\n" % [line_index, decompiled_rom[line_index]]
		else:
			rom_code_text += "%s: %s\n" % [line_index, decompiled_rom[line_index]]
	print_debug(rom_code_text)
	rom_code_display.text = rom_code_text


func update_ram_display() -> void:
	ram_display.text = 'RAM:\n'
	for word_index in range(len(Computer.ram)):
		var current_word = Computer.ram[word_index]
		if current_word != 0:
			ram_display.text += "%s: %s\n" % [str(word_index), str(current_word)]


func _on_UpdateRAMDisplay_pressed() -> void:
	update_ram_display()


func _on_Step_pressed() -> void:
	Computer.run_few(1)
	update_computer_values_display()
	create_image()
	update_texture()
	update_ram_display()
	update_rom_display()


func _on_Timer_timeout() -> void:
	Computer.run_few(loops_per_timeout)

func _input(event: InputEvent) -> void:
	if event is InputEventKey and event.is_pressed():
		if event.scancode == KEY_0: Computer.set_key(KEY_0)
		if event.scancode == KEY_1: Computer.set_key(KEY_1)
		if event.scancode == KEY_2: Computer.set_key(KEY_2)
		if event.scancode == KEY_3: Computer.set_key(KEY_3)
		if event.scancode == KEY_4: Computer.set_key(KEY_4)
		if event.scancode == KEY_5: Computer.set_key(KEY_5)
		if event.scancode == KEY_6: Computer.set_key(KEY_6)
		if event.scancode == KEY_7: Computer.set_key(KEY_7)
		if event.scancode == KEY_8: Computer.set_key(KEY_8)
		if event.scancode == KEY_9: Computer.set_key(KEY_9)
		if event.scancode == KEY_A: Computer.set_key(KEY_A)
		if event.scancode == KEY_B: Computer.set_key(KEY_B)
		if event.scancode == KEY_C: Computer.set_key(KEY_C)
		if event.scancode == KEY_D: Computer.set_key(KEY_D)
		if event.scancode == KEY_E: Computer.set_key(KEY_E)
		if event.scancode == KEY_F: Computer.set_key(KEY_F)
		if event.scancode == KEY_G: Computer.set_key(KEY_G)
		if event.scancode == KEY_H: Computer.set_key(KEY_H)
		if event.scancode == KEY_I: Computer.set_key(KEY_I)
		if event.scancode == KEY_J: Computer.set_key(KEY_J)
		if event.scancode == KEY_K: Computer.set_key(KEY_K)
		if event.scancode == KEY_L: Computer.set_key(KEY_L)
		if event.scancode == KEY_M: Computer.set_key(KEY_M)
		if event.scancode == KEY_N: Computer.set_key(KEY_N)
		if event.scancode == KEY_O: Computer.set_key(KEY_O)
		if event.scancode == KEY_P: Computer.set_key(KEY_P)
		if event.scancode == KEY_Q: Computer.set_key(KEY_Q)
		if event.scancode == KEY_R: Computer.set_key(KEY_R)
		if event.scancode == KEY_S: Computer.set_key(KEY_S)
		if event.scancode == KEY_T: Computer.set_key(KEY_T)
		if event.scancode == KEY_U: Computer.set_key(KEY_U)
		if event.scancode == KEY_V: Computer.set_key(KEY_V)
		if event.scancode == KEY_W: Computer.set_key(KEY_W)
		if event.scancode == KEY_X: Computer.set_key(KEY_X)
		if event.scancode == KEY_Y: Computer.set_key(KEY_Y)
		if event.scancode == KEY_Z: Computer.set_key(KEY_Z)
	if event.is_action_pressed("hack_newline"): Computer.set_key(Computer.KEY_NEWLINE)
	if event.is_action_pressed("hack_backspace"): Computer.set_key(Computer.KEY_NEWLINE)
	if event.is_action_pressed("hack_left_arrow"): Computer.set_key(Computer.KEY_NEWLINE)
	if event.is_action_pressed("hack_up_arrow"): Computer.set_key(Computer.KEY_NEWLINE)
	if event.is_action_pressed("hack_right_arrow"): Computer.set_key(Computer.KEY_NEWLINE)
	if event.is_action_pressed("hack_down_arrow"): Computer.set_key(Computer.KEY_NEWLINE)
	if event.is_action_pressed("hack_home"): Computer.set_key(Computer.KEY_NEWLINE)
	if event.is_action_pressed("hack_end"): Computer.set_key(Computer.KEY_NEWLINE)
	if event.is_action_pressed("hack_page_up"): Computer.set_key(Computer.KEY_NEWLINE)
	if event.is_action_pressed("hack_page_down"): Computer.set_key(Computer.KEY_NEWLINE)
	if event.is_action_pressed("hack_insert"): Computer.set_key(Computer.KEY_NEWLINE)
	if event.is_action_pressed("hack_delete"): Computer.set_key(Computer.KEY_NEWLINE)
	if event.is_action_pressed("hack_escape"): Computer.set_key(Computer.KEY_NEWLINE)
	if event.is_action_pressed("hack_f1"): Computer.set_key(Computer.KEY_F1)
	if event.is_action_pressed("hack_f2"): Computer.set_key(Computer.KEY_F2)
	if event.is_action_pressed("hack_f3"): Computer.set_key(Computer.KEY_F3)
	if event.is_action_pressed("hack_f4"): Computer.set_key(Computer.KEY_F4)
	if event.is_action_pressed("hack_f5"): Computer.set_key(Computer.KEY_F5)
	if event.is_action_pressed("hack_f6"): Computer.set_key(Computer.KEY_F6)
	if event.is_action_pressed("hack_f7"): Computer.set_key(Computer.KEY_F7)
	if event.is_action_pressed("hack_f8"): Computer.set_key(Computer.KEY_F8)
	if event.is_action_pressed("hack_f9"): Computer.set_key(Computer.KEY_F9)
	if event.is_action_pressed("hack_f10"): Computer.set_key(Computer.KEY_F10)
	if event.is_action_pressed("hack_f11"): Computer.set_key(Computer.KEY_F11)
	if event.is_action_pressed("hack_f12"): Computer.set_key(Computer.KEY_F12)


func _on_LoopsSpinBox_value_changed(value: float) -> void:
	loops_per_timeout = value
	timer.start(loops_per_timeout / hertz)


func _on_HertzSpinBox_value_changed(value: float) -> void:
	hertz = value
	timer.start(loops_per_timeout / hertz)


func _on_OpenFileDialog_pressed() -> void:
	var file_dialog: FileDialog = get_node("MarginContainer/VBoxContainer/HBoxContainer2/VBoxContainer/FileDialog")
	file_dialog.popup_centered(Vector2(500, 500))


func _on_FileDialog_file_selected(path: String) -> void:
	var file := File.new()
	file.open(path, File.READ)
	var source: String = file.get_as_text()
	file.close()
	source_text_edit.text = source
	var rom = Assembler.compile_source(source)
	Computer.load_rom(rom)
	Computer.reset()
	update_rom_display()

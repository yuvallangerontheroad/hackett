// i=0

@i
M=0

// n=10
@10
D=A
@n
M=D


// while (i < n) aka
// while (i - n < 0) aka
// while not (i - n >= 0)
(while)

// D = i - n
@i
D=M
@n
D=D-M

// stop if i - n >= 0
@END
D;JGE

// M[i]=-1
@i
A=M
M=-1

// i=i+1
@i
M=M+1

@while
0;JMP

(END)
0;JMP

extends Node


# Hackett, a Godot engine Hack computer emulator.
# Copyright (C) 2021  Yuval Langer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


const VIRTUAL_REGISTER_BASE_ADDRESS = 16


const comp_source_to_comp_bytecode := {
	'0': 0b0000_1010_1000_0000,
	'1': 0b0000_1111_1100_0000,
	'-1': 0b0000_1110_1000_0000,
	'D': 0b0000_0011_0000_0000,
	'A': 0b0000_1100_0000_0000,
	'M': 0b0001_1100_0000_0000,
	'!D': 0b0000_0011_0100_0000,
	'!A': 0b0000_1100_0100_0000,
	'!M': 0b0001_1100_0100_0000,
	'-D': 0b0000_0011_1100_0000,
	'-A': 0b0000_1100_1100_0000,
	'-M': 0b0001_1100_1100_0000,
	'D+1': 0b0000_0111_1100_0000,
	'A+1': 0b0000_1101_1100_0000,
	'M+1': 0b0001_1101_1100_0000,
	'D-1': 0b0000_0011_1000_0000,
	'A-1': 0b0000_1100_1000_0000,
	'M-1': 0b0001_1100_1000_0000,
	'D+A': 0b0000_0000_1000_0000,
	'D+M': 0b0001_0000_1000_0000,
	'D-A': 0b0000_0100_1100_0000,
	'D-M': 0b0001_0100_1100_0000,
	'A-D': 0b0000_0001_1100_0000,
	'M-D': 0b0001_0001_1100_0000,
	'D&A': 0b0000_0000_0000_0000,
	'D&M': 0b0001_0000_0000_0000,
	'D|A': 0b0000_0101_0100_0000,
	'D|M': 0b0001_0101_0100_0000,
}

const jump_source_to_jump_bytecode := {
	'JGT': 0b001,
	'JEQ': 0b010,
	'JGE': 0b011,
	'JLT': 0b100,
	'JNE': 0b101,
	'JLE': 0b110,
	'JMP': 0b111,
}

func invert_dictionary(dictionary: Dictionary) -> Dictionary:
	var new_dictionary := {}
	for key in dictionary:
		new_dictionary[dictionary[key]] = key
	return new_dictionary


var comp_bytecode_to_comp_source := invert_dictionary(comp_source_to_comp_bytecode)
var jump_bytecode_to_jump_source := invert_dictionary(jump_source_to_jump_bytecode)

func compile_dest(dest:String) -> int:
	var bytecode: int = 0
	if dest.find('A') != -1: bytecode += Computer.A_DEST_BIT
	if dest.find('M') != -1: bytecode += Computer.M_DEST_BIT
	if dest.find('D') != -1: bytecode += Computer.D_DEST_BIT
	return bytecode


func decompile_dest(dest:int) -> String:
	var dest_source = ''
	if dest & Computer.A_DEST_BIT:
		dest_source += 'A'
	if dest & Computer.M_DEST_BIT:
		dest_source += 'M'
	if dest & Computer.D_DEST_BIT:
		dest_source += 'D'
	return dest_source


func compile_jump(jump_source: String) -> int:
	return jump_source_to_jump_bytecode[jump_source]


func decompile_jump(jump_bytecode: int) -> String:
	return jump_bytecode_to_jump_source.get(jump_bytecode, '')


func compile_comp(comp: String) -> int:
	return comp_source_to_comp_bytecode[comp]

func decompile_comp(comp_bytecode: int) -> String:
	return comp_bytecode_to_comp_source[comp_bytecode]


func make_builtin_addresses() -> Dictionary:
	var addresses: Dictionary = {
		"SP": Computer.SP_ADDRESS,
		"LCL": Computer.LCL_ADDRESS,
		"ARG": Computer.ARG_ADDRESS,
		"THIS": Computer.THIS_ADDRESS,
		"THAT": Computer.THAT_ADDRESS,
		"SCREEN": Computer.DISPLAY_BASE_ADDRESS,
		"KBD": Computer.KEYBOARD_ADDRESS,
	}
	# Virtual registers.
	for i in range(16):
		addresses["R" + str(i)] = i
	return addresses


func compile_source(source: String) -> PoolIntArray:
	var rom: PoolIntArray = []
	var addresses: Dictionary = make_builtin_addresses()
	var source_lines: PoolStringArray = source.split('\n')
	var bytecode: int

	# Find all labels.
	var pure_source_without_labels: PoolStringArray = []
	for line in source_lines:
		# Clean comments.
		var possible_code = line.split('//')[0].strip_edges()
		if possible_code != '':
			if possible_code[0] == '(':
				var label = possible_code.split('(')[1].split(')')[0]
				addresses[label] = len(pure_source_without_labels)
			else:
				pure_source_without_labels.append(possible_code)

	for source_line in pure_source_without_labels:
		bytecode = make_bytecode_from_source_command(source_line, addresses)
		rom.append(bytecode)

	return rom


func make_bytecode_from_source_command(
	source_command: String,
	addresses: Dictionary
) -> int:
	var bytecode: int
	var comp_then_jump: PoolStringArray
	if source_command[0] == '@':
		var a_address_name: String = source_command.split('@')[1]
		if a_address_name.is_valid_integer():
			bytecode = int(a_address_name)
			return bytecode
		else:
			if not (a_address_name in addresses):
				var a_address_location = VIRTUAL_REGISTER_BASE_ADDRESS
				while a_address_location in addresses.values():
					a_address_location += 1
				addresses[a_address_name] = a_address_location
			bytecode = addresses[a_address_name]
			return bytecode
	else:
		bytecode = 0b1110_0000_0000_0000
		var has_dest = source_command.find('=') != -1
		var has_jump = source_command.find(';') != -1
		if has_dest and has_jump:
			var dest_and_comp_then_jump: PoolStringArray = source_command.split(';')
			var dest_then_comp: PoolStringArray = dest_and_comp_then_jump[0].split('=')
			bytecode += (
				compile_dest(dest_then_comp[0]) +
				compile_comp(dest_then_comp[1]) +
				compile_jump(dest_and_comp_then_jump[1])
			)
			return bytecode
		elif (not has_dest) and has_jump:
			comp_then_jump = source_command.split(';')
			bytecode += (
				compile_comp(comp_then_jump[0]) +
				compile_jump(comp_then_jump[1])
			)
			return bytecode
		elif has_dest and (not has_jump):
			var dest_then_comp = source_command.split('=')
			bytecode += (
				compile_dest(dest_then_comp[0]) +
				compile_comp(dest_then_comp[1])
			)
			return bytecode
		else:
			bytecode += Computer.C_COMMAND_BIT + compile_comp(source_command)
			return bytecode


func from_bytecode_to_binary_code(bytecode: int) -> String:
	var binary_code = ''
	for bit_shift in range(16):
		var current_bit = 1 << bit_shift
		current_bit &= bytecode
		current_bit = current_bit >> bit_shift
		binary_code = str(current_bit) + binary_code
	return binary_code

func from_rom_to_binary_code(rom: PoolIntArray) -> PoolStringArray:
	var binary_codes: PoolStringArray = []
	for bytecode in rom:
		var binary_code = from_bytecode_to_binary_code(bytecode)
		binary_codes.append(binary_code)
	return binary_codes





func decompile(rom: PoolIntArray) -> PoolStringArray:
	var decompiled_source: PoolStringArray = []
	for bytecode in rom:
		var current_source_line := ''
		if bytecode & Computer.C_COMMAND_BIT:
			var dest_code: int = bytecode & Computer.DEST_BITS
			var comp_code: int = bytecode & Computer.COMP_BITS
			var jump_code: int = bytecode & Computer.JUMP_BITS
			var dest_source := decompile_dest(dest_code)
			var comp_source := decompile_comp(comp_code)
			var jump_source := decompile_jump(jump_code)

			if dest_source != '':
				current_source_line += dest_source + '='
			current_source_line += comp_source
			if jump_source != '':
				current_source_line += ';' + jump_source
		else:
			current_source_line = '@' + str(bytecode)
		decompiled_source.append(current_source_line)

	return decompiled_source

extends Node


# Hackett, a Godot engine Hack computer emulator.
# Copyright (C) 2021  Yuval Langer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


const SP_ADDRESS = 0
const LCL_ADDRESS = 1
const ARG_ADDRESS = 2
const THIS_ADDRESS = 3
const THAT_ADDRESS = 4
const DISPLAY_BASE_ADDRESS: int = 0x4000
const DISPLAY_TOP_ADDRESS: int = 0x5FFF
const KEYBOARD_ADDRESS: int = 0x6000
const RAM_SIZE: int = 0x8000
const REGULAR_VARIABLE_START_ADDRESS: int = 16
const C_COMMAND_BIT: int = 0b1000_0000_0000_0000
const JUMP_BITS: int =  0b0000_0000_0000_0111
const DEST_BITS: int =  0b0000_0000_0011_1000
const M_DEST_BIT: int = 0b0000_0000_0000_1000
const D_DEST_BIT: int = 0b0000_0000_0001_0000
const A_DEST_BIT: int = 0b0000_0000_0010_0000
const COMP_BITS: int =  0b0001_1111_1100_0000
const JUMP_JGT_BITS: int = 0b001
const JUMP_JEQ_BITS: int = 0b010
const JUMP_JGE_BITS: int = 0b011
const JUMP_JLT_BITS: int = 0b100
const JUMP_JNE_BITS: int = 0b101
const JUMP_JLE_BITS: int = 0b110
const JUMP_JMP_BITS: int = 0b111

const KEY_NEWLINE := 128
const KEY_BACKSPACE := 129
const KEY_LEFT_ARROW := 130
const KEY_UP_ARROW := 131
const KEY_RIGHT_ARROW := 132
const KEY_DOWN_ARROW := 133
const KEY_HOME := 134
const KEY_END := 135
const KEY_PAGE_UP := 136
const KEY_PAGE_DOWN := 137
const KEY_INSERT := 138
const KEY_DELETE := 139
const KEY_ESCAPE := 140
const KEY_F1 := 141
const KEY_F2 := 142
const KEY_F3 := 143
const KEY_F4 := 144
const KEY_F5 := 145
const KEY_F6 := 146
const KEY_F7 := 147
const KEY_F8 := 148
const KEY_F9 := 149
const KEY_F10 := 150
const KEY_F11 := 151
const KEY_F12 := 152

var program_counter: int = 0
var a_register: int
var d_register: int
var ram: PoolIntArray = []
var rom: PoolIntArray = []

var number_of_operations_thus_far: int = 0

#var display_mutex: Mutex
var changed_pixels: Array = []


func _init() -> void:
#	display_mutex = Mutex.new()
	ram.resize(RAM_SIZE)
	for i in range(ram.size()):
		ram[i] = 0
	number_of_operations_thus_far = 0


func load_rom(new_rom: PoolIntArray) -> void:
	self.rom = new_rom


func set_key(keycode: int) -> void:
	ram[KEYBOARD_ADDRESS] = keycode


func run() -> void:
	while true:
		print_debug(program_counter)
		run_assembly_command(rom[program_counter])
		number_of_operations_thus_far += 1


func run_few(n: int) -> void:
	for _i in range(n):
		if program_counter < len(rom):
			run_assembly_command(rom[program_counter])
			number_of_operations_thus_far += 1


func run_assembly_command(command: int) -> void:
	if not (command & C_COMMAND_BIT):
		a_register = command
		program_counter += 1
	else:
		var comp_result: int = compute(command & COMP_BITS)

		if command & JUMP_BITS == 0:
			program_counter += 1
		else:
			make_jump(comp_result, command & JUMP_BITS)

		if command & M_DEST_BIT:
			if DISPLAY_BASE_ADDRESS <= a_register and a_register <= DISPLAY_TOP_ADDRESS:
#				print_debug("display ", a_register)
#				display_mutex.lock()
#				emit_signal("display_update", a_register, comp_result)
#				print_debug("computation result: %s" % comp_result)
				changed_pixels.append([a_register, comp_result])
#				display_mutex.unlock()
			ram[a_register] = comp_result
		if command & A_DEST_BIT:
			if comp_result < 0:
				print("WARNING! A-register cannot be assigned negative values.")
			a_register = comp_result
		if command & D_DEST_BIT:
			d_register = comp_result


func compute(comp: int) -> int:
	if comp == 0b1010_1000_0000:
		return 0
	elif comp == 0b1111_1100_0000:
		return 1
	elif comp == 0b1110_1000_0000:
		return -1
	elif comp == 0b0011_0000_0000:
		return d_register
	elif comp == 0b0_1100_0000_0000:
		return a_register
	elif comp == 0b1_1100_0000_0000:
		return ram[a_register]
	elif comp == 0b0011_0100_0000:
		# XXX This will implode due to ints here being 64 bit rather than 16 bit. Or will it…?
		return ~d_register
	elif comp == 0b0_1100_0100_0000:
		# XXX This will implode due to ints here being 64 bit rather than 16 bit. Or will it…?
		return ~a_register
	elif comp == 0b1_1100_0100_0000:
		# XXX This will implode due to ints here being 64 bit rather than 16 bit. Or will it…?
		return ~ram[a_register]
	elif comp == 0b0011_1100_0000:
		return -d_register
	elif comp == 0b0_110_011_000_000:
		return -a_register
	elif comp == 0b1_110_011_000_000:
		return -ram[a_register]
	elif comp == 0b0111_1100_0000:
		return d_register + 1
	elif comp == 0b0_1101_1100_0000:
		return a_register + 1
	elif comp == 0b1_1101_1100_0000:
		return ram[a_register] + 1
	elif comp == 0b0011_1000_0000:
		return d_register - 1
	elif comp == 0b0_1100_1000_0000:
		return a_register - 1
	elif comp == 0b1_1100_1000_0000:
		return ram[a_register] - 1
	elif comp == 0b0_0000_1000_0000:
		return d_register + a_register
	elif comp == 0b1_0000_1000_0000:
		return d_register + ram[a_register]
	elif comp == 0b0_0100_1100_0000:
		return d_register - a_register
	elif comp == 0b1_0100_1100_0000:
		return d_register - ram[a_register]
	elif comp == 0b0_0001_1100_0000:
		return a_register - d_register
	elif comp == 0b1_0001_1100_0000:
		return ram[a_register] - d_register
	elif comp == 0b0_0000_0000_0000:
		return d_register & a_register
	elif comp == 0b1_0000_0000_0000:
		return d_register & ram[a_register]
	elif comp == 0b0_0101_0100_0000:
		return d_register | a_register
	elif comp == 0b1_0101_0100_0000:
		return d_register | ram[a_register]
	else:
		return 0


func make_jump(comp_result: int, jump: int) -> void:
	if jump == JUMP_JGT_BITS and comp_result > 0:
		program_counter = a_register
	elif jump == JUMP_JEQ_BITS and comp_result == 0:
		program_counter = a_register
	elif jump == JUMP_JGE_BITS and comp_result >= 0:
		program_counter = a_register
	elif jump == JUMP_JLT_BITS and comp_result < 0:
		program_counter = a_register
	elif jump == JUMP_JNE_BITS and comp_result != 0:
		program_counter = a_register
	elif jump == JUMP_JLE_BITS and comp_result <= 0:
		program_counter = a_register
	elif jump == JUMP_JMP_BITS:
		program_counter = a_register
	else:
		program_counter += 1


func reset() -> void:
	program_counter = 0
	a_register = 0
	d_register = 0
	for i in range(len(ram)):
		ram[i] = 0


func make_cool_load_screen_source_1(step: int) -> String:
	var source: String = ""
	for divider in range(step):
		for i in range(0x4000 + divider, 0x6000, step):
#		for i in range(0x4000 + divider, 0x4000, 1):
			source += "@0\n"
			source += "D=A\n"
			source += "D=D-1\n"
			source += "@" + str(i) + "\n"
			source += "M=D\n"

	source += "(END)\n"
	source += "@END\n"
	source += "0;JMP\n"

	return source

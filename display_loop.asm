// i=SCREEN=0x4000=16384

@SCREEN
D=A
@i
M=D

// n=SCREEN+256*512//16=0x4000+0x2000=0x6000=24576
@24576
D=A
@n
M=D


// while (i < n) aka
// while (i - n < 0) aka
// while not (i - n >= 0)
(while)

// D = i - n
@i
D=M
@n
D=D-M

// stop if i - n >= 0
@END
D;JGE

// M[i]=-1
@i
A=M
M=-1

// i=i+1
@i
M=M+1

@while
0;JMP

(END)
0;JMP
